<?php
/**
 * Created by PhpStorm.
 * User: Jamie
 * Date: 3/20/2018
 * Time: 8:24 PM
 */

$searchTerm = '';

if(isset($_POST['searchTerm'])) {
    $searchTerm = $_POST['searchTerm'];
}

$countries = json_decode(makeApiRequest('name/' . $searchTerm));

//If the input is 2 or 3 characters, check the API for a country matching that code.
if(strlen($searchTerm) === 2 || strlen($searchTerm) === 3) {
    $countryByCode = makeApiRequest('alpha/' . $searchTerm);

    if(isset($countryByCode->name)) {
        $foundCountry = false;
        foreach($countries as $country) {
            if($country->name === $countryByCode->name) {
                $foundCountry = true;
                break;
            }
        }

        if(!$foundCountry) {
            $countries[] = $countryByCode;
        }
    }
}

$metaData = new stdClass();
$regions = [];
$subregions = [];

$message = '';
$hasError = false;
if(isset($countries->message)) {
    $message = $countries->message;
    $hasError = true;
    $countries = [];
} else {
    usort($countries, 'countrySort');
    $countries = array_slice($countries, 0, 50);

    $tempRegions = [];
    $tempSubregions = [];
    foreach($countries as $country) {
        if(isset($tempRegions[$country->region])) {
            $tempRegions[$country->region]++;
        } else {
            $tempRegions[$country->region] = 1;
        }

        if(isset($tempSubregions[$country->subregion])) {
            $tempSubregions[$country->subregion]++;
        } else {
            if(!empty($country->subregion)) {
                $tempSubregions[$country->subregion] = 1;
            }
        }

        $country->population = number_format($country->population);

        $languages = [];
        foreach($country->languages as $language) {
            $languages[] = $language->name;
        }

        $country->languageString = implode(', ', $languages);
    }

    foreach($tempRegions as $name => $count) {
        $r = new stdClass();
        $r->name = $name;
        $r->count = $count;
        $regions[] = $r;
    }
    usort($regions, 'countrySort');

    foreach($tempSubregions as $name => $count) {
        $r = new stdClass();
        $r->name = $name;
        $r->count = $count;
        $subregions[] = $r;
    }
    usort($subregions, 'countrySort');
}

$metaData->numberOfCountries = count($countries);
$metaData->regions = $regions;
$metaData->subregions = $subregions;

$response = new stdClass();
$response->message = $message;
$response->error = $hasError;
$response->countries = $countries;
$response->metaData = $metaData;

echo json_encode($response);

/**
 * @param $url
 * @return mixed
 */
function makeApiRequest($url) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://restcountries.eu/rest/v2/' . $url,
        CURLOPT_SSL_VERIFYPEER => 0
    ));

    $response = curl_exec($curl);

    if($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }

    curl_close($curl);

    return $response;
}

/**
 * @param $a
 * @param $b
 * @return int
 */
function countrySort($a, $b) {
    return strcmp($a->name, $b->name);
}