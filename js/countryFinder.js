/**
 * Created by Jamie on 3/20/2018.
 */
(function () {
    $(document).ready(function() {
        controlForm();
    });

    function controlForm() {
        var $form = $('.country-finder-form');
        var $input = $form.find('.country-finder-form-input');

        $form.on('submit', function () {
            var searchTerm = $input.val();

            if (searchTerm.length > 0) {
                getApiResults(searchTerm)
                    .done(function(results) {
                        if(results.error) {
                            throwFormError(results.message);
                        } else {
                            displayCountries(results.countries, results.metaData);
                        }
                    });
            } else {
                throwFormError('Please enter a search term');
            }

            return false;
        });
    }

    function displayCountries(countries, metaData) {
        hideFormError();

        var countryContainer = $('.country-container');
        var metaDataContainer = $('.metadata-container');

        var countryTemplate = $.templates('#countryTemplate');
        var metaDataTemplate = $.templates('#metaDataTemplate');

        var html = countryTemplate.render(countries);
        countryContainer.html(html);

        html = metaDataTemplate.render(metaData);
        metaDataContainer.html(html);

        var $metaDataSection = $('.metadata-section');
        $metaDataSection.addClass('grey-section');
    }

    function throwFormError(message) {
        var $errorMessage = $('.country-finder-form-error-message');
        $errorMessage.html(message);
    }

    function hideFormError() {
        throwFormError('');
    }

    function getApiResults(searchTerm) {
        return $.ajax({
            url: 'getApiResults.php',
            type: 'POST',
            data: {
                searchTerm: searchTerm
            },
            dataType: 'json'
        });
    }
})();