# CountryFinder

Allows users to search for countries by name, alpha code 2, and alpha code 3.

## To Run the PHP Server
Open your terminal, and switch to the root directory of this application

Run the command: `php -S localhost:8000`